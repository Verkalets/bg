Bg::Application.routes.draw do

  resources :merchant_children

  resources :merchant_pers

  resources :templates
  root to: "templates#index"
  resources :income
  resources :expence

  get 'public/data/:id' => 'templates#download', as: :download

end
