working_directory "/home/deploy/mt940.ch/current"

pid "/home/deploy/mt940.ch/shared/pids/unicorn.pid"

stderr_path "/home/deploy/mt940.ch/shared/log/unicorn.log"
stdout_path "/home/deploy/mt940.ch/shared/log/unicorn.log"

listen "/tmp/unicorn.mt940.sock"

worker_processes 2

timeout 30
