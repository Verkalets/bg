require 'rails_helper'

RSpec.describe "merchant_pers/show", :type => :view do
  before(:each) do
    @merchant_per = assign(:merchant_per, MerchantPer.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
