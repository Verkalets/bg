require 'rails_helper'

RSpec.describe "merchant_pers/new", :type => :view do
  before(:each) do
    assign(:merchant_per, MerchantPer.new(
      :name => "MyString"
    ))
  end

  it "renders new merchant_per form" do
    render

    assert_select "form[action=?][method=?]", merchant_pers_path, "post" do

      assert_select "input#merchant_per_name[name=?]", "merchant_per[name]"
    end
  end
end
