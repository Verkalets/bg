require 'rails_helper'

RSpec.describe "merchant_pers/edit", :type => :view do
  before(:each) do
    @merchant_per = assign(:merchant_per, MerchantPer.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit merchant_per form" do
    render

    assert_select "form[action=?][method=?]", merchant_per_path(@merchant_per), "post" do

      assert_select "input#merchant_per_name[name=?]", "merchant_per[name]"
    end
  end
end
