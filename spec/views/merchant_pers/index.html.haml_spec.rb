require 'rails_helper'

RSpec.describe "merchant_pers/index", :type => :view do
  before(:each) do
    assign(:merchant_pers, [
      MerchantPer.create!(
        :name => "Name"
      ),
      MerchantPer.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of merchant_pers" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
