require 'rails_helper'

RSpec.describe "merchant_children/index", :type => :view do
  before(:each) do
    assign(:merchant_children, [
      MerchantChild.create!(
        :name => "Name"
      ),
      MerchantChild.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of merchant_children" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
