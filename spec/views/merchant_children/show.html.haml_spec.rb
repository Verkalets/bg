require 'rails_helper'

RSpec.describe "merchant_children/show", :type => :view do
  before(:each) do
    @merchant_child = assign(:merchant_child, MerchantChild.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
