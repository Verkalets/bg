require 'rails_helper'

RSpec.describe "merchant_children/new", :type => :view do
  before(:each) do
    assign(:merchant_child, MerchantChild.new(
      :name => "MyString"
    ))
  end

  it "renders new merchant_child form" do
    render

    assert_select "form[action=?][method=?]", merchant_children_path, "post" do

      assert_select "input#merchant_child_name[name=?]", "merchant_child[name]"
    end
  end
end
