require 'rails_helper'

RSpec.describe "merchant_children/edit", :type => :view do
  before(:each) do
    @merchant_child = assign(:merchant_child, MerchantChild.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit merchant_child form" do
    render

    assert_select "form[action=?][method=?]", merchant_child_path(@merchant_child), "post" do

      assert_select "input#merchant_child_name[name=?]", "merchant_child[name]"
    end
  end
end
