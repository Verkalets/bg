require "rails_helper"

RSpec.describe MerchantPersController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/merchant_pers").to route_to("merchant_pers#index")
    end

    it "routes to #new" do
      expect(:get => "/merchant_pers/new").to route_to("merchant_pers#new")
    end

    it "routes to #show" do
      expect(:get => "/merchant_pers/1").to route_to("merchant_pers#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/merchant_pers/1/edit").to route_to("merchant_pers#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/merchant_pers").to route_to("merchant_pers#create")
    end

    it "routes to #update" do
      expect(:put => "/merchant_pers/1").to route_to("merchant_pers#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/merchant_pers/1").to route_to("merchant_pers#destroy", :id => "1")
    end

  end
end
