require "rails_helper"

RSpec.describe MerchantChildrenController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/merchant_children").to route_to("merchant_children#index")
    end

    it "routes to #new" do
      expect(:get => "/merchant_children/new").to route_to("merchant_children#new")
    end

    it "routes to #show" do
      expect(:get => "/merchant_children/1").to route_to("merchant_children#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/merchant_children/1/edit").to route_to("merchant_children#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/merchant_children").to route_to("merchant_children#create")
    end

    it "routes to #update" do
      expect(:put => "/merchant_children/1").to route_to("merchant_children#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/merchant_children/1").to route_to("merchant_children#destroy", :id => "1")
    end

  end
end
