module TemplatesHelper

  ######################
  # Get iterator count #
  ######################

  def iterator_count_weekly(from_date, to_date)
    @fd = from_date.to_date
    @td = to_date.to_date
    @per = 7
    @resd = (@td - @fd) / @per
    return @resd.ceil.to_s
  end

  def iterator_count_monthly(from_date, to_date)
    @fd = from_date.to_date
    @td = to_date.to_date
    @per = 30
    @resd = (@td - @fd) / @per
    return @resd.ceil.to_s
  end

  ######------#######

  ##################
  # Get Income sum #
  ##################

  def get_all_income_amount
    @sum = 0
    @template.income.each do |e|
      s = e.ein.to_f
      @sum = @sum + s
    end

    puts @sum
  end

  ######------#######


  ###################
  # Get Expence sum #
  ###################

  def get_all_expence_amount

    @sum_f = 0
    @template.expence.each do |e|
      s = e.fix_amount.to_f
      @sum_f = @sum_f + s
    end

    @sum_a = 0

    @template.expence.each do |e|
      s = e.amount_finish.to_f
      t = e.t_finish.to_i
      @sum_a = @sum_a + (s * t)
    end

    @sum_r = 0
    @template.expence.each do |e|
      s = e.range_finish.to_f
      @sum_r = @sum_r + s
    end

    @amount_end = @sum_f + @sum_a + @sum_r

    @f_am = @template.start_amount.to_f - @amount_end.to_f

    return @f_am
  end

  def to_bank_number number
    @number = number.to_s
    @s = sprintf("%10.2f", @number)
    @ss = @s.gsub!(' ', '0').gsub!('.', ',')
  end


  def add_expence_row

    @w_iterator = iterator_count_weekly(@template.from_date, @template.to_date)
    @m_iterator = iterator_count_monthly(@template.from_date, @template.to_date)

    @return_string = ''

    @template.expence.each do |e|
        if e.type === 'reccuring' && e.range_from != '' && e.range_to != '' && e.fix_amount === ''
          rrn = Random.new
          #@rr = rrn.rand(e.range_from.to_f..e.range_to.to_f)
          @ex_value = rrn.rand(e.range_from.to_f..e.range_to.to_f)
          @ex_name = e.rec_name
        elsif e.type === 'reccuring' && e.fix_amount != '' && e.range_from === '' && e.range_to === ''
          @ex_value = e.fix_amount
          @ex_name = e.rec_name
        elsif e.type === 'merchant_list' && e.amount_from != '' && e.amount_to != '' && e.min_t != '' && e.max_t != ''
          @ex_value = e.amount_finish
          #@ex_name = e.list_item

          @mech = MerchantPer.all

          @list_item = @mech.where(name: e.list_item).first
          @ext = @list_item.merchant_children.sort_by{rand}.last
          @ex_name = @ext.name


        end

        @r = ''
        if e.peridiocicy === 'weekly'

          @range_end = @w_iterator.to_i

          @r =''
          @day = @template.from_date.to_date.strftime("%y%m%d")

          (1..@range_end).each do |i|
            if e.type === 'reccuring' && e.range_from != '' && e.range_to != '' && e.fix_amount === ''

              rrn = Random.new
              @day = @day.to_date + 1.week
              @ren_day = @day.strftime("%y%m%d")
              @ren_sday = @day.strftime("%m%d")

              @ren_str = "
:61:" +@ren_day.to_s+@ren_sday+"C"+ to_bank_number(rrn.rand(e.range_from.to_f..e.range_to.to_f))+  "FMSCNONREF//NONREF
:86:"+@ex_name.to_s+"\n"

            elsif e.type === 'reccuring' && e.fix_amount != '' && e.range_from === '' && e.range_to === ''
              rrn = Random.new
              @day = @day.to_date + 1.week
              @ren_day = @day.strftime("%y%m%d")
              @ren_sday = @day.strftime("%m%d")

              @ren_str = "
:61:" +@ren_day.to_s+@ren_sday+"C"+ to_bank_number(e.fix_amount.to_f)+  "FMSCNONREF//NONREF
:86:"+@ex_name.to_s+"\n"
            end


            @r = @r + @ren_str

          end


        elsif e.peridiocicy === 'monthly'

          @range_end = @m_iterator.to_i

          @r =''
          @day = @template.from_date.to_date.strftime("%y%m%d")

          (1..@range_end).each do |i|
            if e.type === 'reccuring' && e.range_from != '' && e.range_to != '' && e.fix_amount === ''

              rrn = Random.new
              @day = @day.to_date + 1.months
              @ren_day = @day.strftime("%y%m%d")
              @ren_sday = @day.strftime("%m%d")

              @ren_str = "
:61:" +@ren_day.to_s+@ren_sday+"C"+ to_bank_number(rrn.rand(e.range_from.to_f..e.range_to.to_f))+  "FMSCNONREF//NONREF
:86:"+@ex_name.to_s+"\n"

            elsif e.type === 'reccuring' && e.fix_amount != '' && e.range_from === '' && e.range_to === ''
              rrn = Random.new
              @day = @day.to_date + 1.months
              @ren_day = @day.strftime("%y%m%d")
              @ren_sday = @day.strftime("%m%d")

              @ren_str = "
:61:" +@ren_day.to_s+@ren_sday+"C"+ to_bank_number(e.fix_amount.to_f)+  "FMSCNONREF//NONREF
:86:"+@ex_name.to_s+"\n"

            elsif e.type === 'merchant_list'

              rmr = Random.new
              @range_end = rmr.rand(e.min_t.to_f..e.max_t.to_f)
              @r =''
              @day = @template.from_date.to_date.strftime("%y%m%d")

               (1..@range_end).each do |i|

                  @mech = MerchantPer.all

                  @list_item = @mech.where(name: e.list_item).first
                  @ext = @list_item.merchant_children.sort_by{rand}.last
                  @ex_name = @ext.name



                   @day = @day.to_date + 4.days
                   rma = Random.new
                   @day = @day.to_date
                   @ren_day = @day.strftime("%y%m%d")
                   @ren_sday = @day.strftime("%m%d")

                   @ren_str = "
:61:" +@ren_day.to_s+@ren_sday+"C"+ to_bank_number(rma.rand(e.amount_from.to_f..e.amount_to.to_f))+  "FMSCNONREF//NONREF
:86:"+@ex_name.to_s+"NON FIXED "+"\n"

                @r = @r + @ren_str # Fixed Merchant list
               end

            end

            @r = @r + @ren_str

          end

        elsif e.type === 'merchant_list'

          rmr = Random.new
          @step = rmr.rand(1..e.min_t.to_i)
          @r =''
          @day = @template.from_date.to_date.strftime("%y%m%d")

           (@template.from_date.to_date..@template.to_date.to_date).step(@step.to_i) do |i|
              puts ">>>>#{i}<<<<"
              @mech = MerchantPer.all

              @list_item = @mech.where(name: e.list_item).first
              @ext = @list_item.merchant_children.sort_by{rand}.last
              @ex_name = @ext.name

              @diff_time = @template.to_date.to_date - @template.from_date.to_date

               @ran_day = Random.new

               @ran_day_af = @ran_day.rand(1..@diff_time.to_i)

              random_ivan = Random.new
              random_ivan_af = random_ivan.rand(1..e.min_t.to_i)

               @day = i + random_ivan_af


               rma = Random.new

               @day = @day.to_date
               @ren_day = @day.strftime("%y%m%d")
               @ren_sday = @day.strftime("%m%d")

               @ren_str = "
:61:" +@ren_day.to_s+@ren_sday+"C"+ to_bank_number(rma.rand(e.amount_from.to_f..e.amount_to.to_f))+  "FMSCNONREF//NONREF
:86:"+@ex_name.to_s+"\n"

            @r = @r + @ren_str
           end



        end

        @return_string = @return_string + @r
      #Handle reccuring


    end

  return @return_string

  end

  def add_income_row
    @return_string = ''

    @template.income.each do |i|

      @in_value = i.ein
      @in_name = i.name_arb
      @return_string =  @return_string +"
:61:" +@template.from_date.to_date.strftime("%y%m%d") +@template.from_date.to_date.strftime("%m%d")+"D"+to_bank_number(@in_value.to_f)+"FMSCNONREF//NONREF
:86:"+@in_name+"\n"
    end

    return @return_string

  end

  ######------#######

  def file_handler
    @content = ':20:'+@template.from_date.to_date.strftime("%Y%m%d")+to_bank_number(@template.start_amount.to_f)+'
:25:8401/'+@template.account_name+'
:28C:25/1
:60F:C101231CHF000000000000000
'+add_expence_row+add_income_row+'
:62F:C'+@template.to_date.to_date.strftime("%y%m%d")+'CHF'+to_bank_number(get_all_expence_amount.to_f)+'
:86:22@@VN@@ @@NN@@
'
    out_file = File.new("public/data/"+ params[:id] + ".sta", "w")
    out_file.write(@content)
    out_file.close
    @file_url = request.protocol+request.host_with_port+'/'+ File.path(out_file)
    @file_content = File.read(out_file)

    return @file_content
  end


end
