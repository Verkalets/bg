class Template
  include Mongoid::Document
  field :bank, type: String
  field :account_name, type: String
  field :start_amount, type: String
  field :from_date, type: String
  field :to_date, type: String

  has_many :income, class_name: 'Income', inverse_of: :template
  has_many :expence, class_name: 'Expence', inverse_of: :template
end
