class MerchantChild
  include Mongoid::Document
  field :name, type: String
  field :merchant_per_id, type: String

  belongs_to :merchant_per, class_name: 'MerchantPer', inverse_of: :merchant_child
end
