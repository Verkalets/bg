class MerchantPer
  include Mongoid::Document
  field :name, type: String
  field :merchant_child_id, type: String

  has_many :merchant_children, class_name: 'MerchantChild', inverse_of: :merchant_per
end
