class Expence
  include Mongoid::Document
  field :type, type: String
  field :peridiocicy, type: String
  field :rec_name, type: String
  field :rec_type, type: String
  field :fix_amount, type: String
  field :range_from, type: String
  field :range_to, type: String
  field :list_item, type: String
  field :list_child, type: String
  field :min_t, type: String
  field :max_t, type: String
  field :amount_from, type: String
  field :amount_to, type: String
  field :amount_finish, type: String
  field :range_finish, type: String
  field :t_finish, type: String

  belongs_to :template, class_name: 'Template', inverse_of: :expence

  after_validation :set_range
  after_validation :set_amount
  after_validation :set_t
  after_validation :set_list_item


  def set_range
    rrn = Random.new
    @rr = rrn.rand(self.range_from.to_f..self.range_to.to_f)
    self.range_finish = @rr
  end

  def set_amount
    ran = Random.new
    @ra = ran.rand(self.amount_from.to_f..self.amount_to.to_f)
    self.amount_finish = @ra
  end

  def set_t
    rtn = Random.new
    @rt = rtn.rand(self.min_t.to_f..self.max_t.to_f)
    self.t_finish = @rt
  end

  def set_list_item
    @mech = MerchantPer.all

    @list_item = @mech.where(name: self.list_item).first
    @ext = @list_item.merchant_children.last
    self.list_child = @ext.name 
  end



end
