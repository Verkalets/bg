class Income
  include Mongoid::Document
  field :ein, type: String
  field :name_arb, type: String

  belongs_to :template, class_name: 'Template', inverse_of: :income
end
