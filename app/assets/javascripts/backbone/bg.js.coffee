#= require_self
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./views
#= require_tree ./routers

window.Bg =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}

  Vent: _.clone(Backbone.Events)

  initialize: ->
    new App.Routers.MainRouter()
    Backbone.history.start()
