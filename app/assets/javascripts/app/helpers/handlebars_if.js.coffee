Handlebars.registerHelper "ifCond", (v1, v2, options) ->
  return options.fn(this)  if v1 is v2
  options.inverse this
