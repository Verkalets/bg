class App.Views.ExpenceDetails extends Backbone.View

  template: HandlebarsTemplates['app/templates/expence_details']


  events:
    'click #delete_expence': 'destroyExpence'
    'click #update_expence': 'updateExpence'

  updateExpence: (e) ->
    e.preventDefault()
    App.Vent.trigger 'expence:update', @model

  destroyExpence: (e) ->
    e.preventDefault()
    return unless confirm('Are you sure?')
    @model.destroy
      success: ->
        App.Vent.trigger 'expence:destroy', @model
        alert 'deleted'

  initialize: ->
    @listenTo @model, 'sync', @render
    @model.fetch()

  render: ->
    @$el.html(@template(@model.toJSON()))
    @
