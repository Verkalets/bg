class App.Views.Income extends Backbone.View

  template: HandlebarsTemplates['app/templates/income']

  tagName: 'li'

  events:
    'click #show_income_details': 'showIncomeDetails'

  initialize: ->
    @listenTo @model, 'destroy', @remove
    @listenTo @model, 'change', @render

  showIncomeDetails: (e) ->
    e.preventDefault()
    App.Vent.trigger 'income:show', @model

  render: ->
    @$el.html(@template(@model.toJSON()))
    @
