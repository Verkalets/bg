class App.Views.Content extends Backbone.View

  className: "content"

  template: HandlebarsTemplates['app/templates/content']

  render: ->
    @$el.html(@template())
    @

  initialize: ->

    @listenTo App.Vent, "income:create", @swapMiddleNewIncomeToNewIncome
    @listenTo App.Vent, "income:new", @swapMiddleToNewIncome
    @listenTo App.Vent, "income:show", @swapTopToIncomeShow
    @listenTo App.Vent, "income:destroy", @swapTopToEmpty
    @listenTo App.Vent, "income:update", @swapTopToUpdate

    @listenTo App.Vent, "expence:create", @swapBottomNewExpenceToNewExpence
    @listenTo App.Vent, "expence:new", @swapBottomToNewExpence
    @listenTo App.Vent, "expence:show", @swapTopToExpenceShow
    @listenTo App.Vent, "expence:destroy", @swapTopToEmpty
    @listenTo App.Vent, "expence:update", @swapTopToUpdateExpence


  swapTopToUpdateExpence: (model)->
    @swapTop(new App.Views.NewExpence({ model: model }))

  swapTopToEmptyExpence: ->
    @swapTop(new App.Views.EmptyEditor())

  swapTopToExpenceShow: (model) ->
    @swapTop(new App.Views.ExpenceDetails({ model: model }))

  swapTopToUpdate: (model)->
    @swapTop(new App.Views.NewIncome({ model: model }))

  swapTopToEmpty: ->
    @swapTop(new App.Views.EmptyEditor())

  swapTopToIncomeShow: (model) ->
    @swapTop(new App.Views.IncomeDetails({ model: model }))

  swapMiddleNewIncomeToNewIncome: ->
    @swapMiddleNewIncome(new App.Views.NewIncome({ model: new App.Models.Income }))
    @swapTop(new App.Views.EmptyEditor())
  swapMiddleToNewIncome: ->
    @swapMiddleNewIncome(new App.Views.NewIncome({ model: new App.Models.Income }))

  renderIncomes: ->
    @swapMiddleList(new App.Views.Incomes({ collection: new App.Collections.Incomes }))

  swapBottomNewExpenceToNewExpence: ->
    @swapBottomNewExpence(new App.Views.NewExpence({ model: new App.Models.Expence }))
    @swapTop(new App.Views.EmptyEditor())

  swapBottomToNewExpence: ->
    @swapBottomNewExpence(new App.Views.NewExpence({ model: new App.Models.Expence }))
    swapBottomNewExpence

  renderExpences: ->
    @swapBottomList(new App.Views.Expences({ collection: new App.Collections.Expences }))
  #Swap helpers

  swapTop: (v) ->
    @changeCurrentTopView(v)
    @$('#top').html(@currentTopView.render().el)

  changeCurrentTopView: (v) ->
    @currentTopView.remove() if @currentTopView
    @currentTopView = v


  swapMiddleList: (v) ->
    @changeCurrentMiddleListView(v)
    @$('#list_incomes').html(@currentMiddleListView.render().el)

  changeCurrentMiddleListView: (v) ->
    @currentMiddleListView.remove() if @currentMiddleListView
    @currentMiddleListView = v

  swapMiddleNewIncome: (v) ->
    @changeCurrentMiddleNewIncomeView(v)
    @$('#new_incomes').html(@currentMiddleNewIncomeView.render().el)

  changeCurrentMiddleNewIncomeView: (v) ->
    @currentMiddleNewIncomeView.remove() if @currentMiddleNewIncomeView
    @currentMiddleNewIncomeView = v


  swapBottomList: (v) ->
    @changeCurrentBottomListView(v)
    @$('#list_expences').html(@currentBottomListView.render().el)

  changeCurrentBottomListView: (v) ->
    @currentBottomListView.remove() if @currentBottomListView
    @currentBottomListView = v

  swapBottomNewExpence: (v) ->
    @changeCurrentBottomNewExpenceView(v)
    @$('#new_expences').html(@currentBottomNewExpenceView.render().el)

  changeCurrentBottomNewExpenceView: (v) ->
    @currentBottomNewExpenceView.remove() if @currentBottomNewExpenceView
    @currentBottomNewExpenceView = v
