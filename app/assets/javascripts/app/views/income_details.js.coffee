class App.Views.IncomeDetails extends Backbone.View

  template: HandlebarsTemplates['app/templates/income_details']


  events:
    'click #delete_income': 'destroyIncome'
    'click #update_income': 'updateIncome'



  updateIncome: (e) ->
    e.preventDefault() 
    App.Vent.trigger 'income:update', @model

  destroyIncome: (e) ->
    e.preventDefault()
    return unless confirm('Are you sure?')
    @model.destroy
      success: ->
        App.Vent.trigger 'income:destroy', @model

  initialize: ->
    @listenTo @model, 'sync', @render
    @model.fetch()

  render: ->
    @$el.html(@template(@model.toJSON()))
    @
