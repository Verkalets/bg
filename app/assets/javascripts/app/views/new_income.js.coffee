class App.Views.NewIncome extends Backbone.View

  className: "content"

  template: HandlebarsTemplates['app/templates/new_income']

  render: ->
    @$el.html(@template(@model.toJSON()))
    @

  tempalateUrl = document.URL.toString().substr(document.URL.toString().indexOf("es/") + 3).split("/edit")[0]

  events:
    'click button': "saveIncome"

  saveIncome: (e) ->
    e.preventDefault()
    @model.set name_arb: @$('#name_arb').val()
    @model.set ein: @$('#ein').val()
    @model.set template_id: tempalateUrl;
    @model.save {},
      success: (model) ->
        App.Vent.trigger 'income:create', model
      error: ->
        alert 'error'
