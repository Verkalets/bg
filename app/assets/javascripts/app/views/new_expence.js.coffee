class App.Views.NewExpence extends Backbone.View

  className: "content"

  template: HandlebarsTemplates['app/templates/new_expence']

  render: ->
    @$el.html(@template(@model.toJSON()))
    @

  tempalateUrl = document.URL.toString().substr(document.URL.toString().indexOf("es/") + 3).split("/edit")[0]

  events:
    'click button': "saveExpence"
    'change #type': "changeType"
    'change #amount_type': 'changeAmount'


  changeAmount: ->
    if @$('#amount_type').val() is 'fix_amount'
      $('.fix_amount').show()
      $('.range_amount').hide()
    if @$('#amount_type').val() is 'range'
      $('.range_amount').show()
      $('.fix_amount').hide()

  changeType: ->
    if @$('#type').val() is 'reccuring'
      $('.reccuring').show()
      $('.merchant_list').hide()
    if @$('#type').val() is 'merchant_list'
      $('.merchant_list').show()
      $('.reccuring').hide()
      setTimeout (->
        item = $('#gethis').html()
        $('#ml').html(item)
      ), 10



  saveExpence: (e) ->
    e.preventDefault()
    @model.set type: @$('#type').val()
    @model.set peridiocicy: @$('#peridiocicy').val()
    @model.set rec_name: @$('#rec_name').val()
    @model.set rec_type: @$('#rec_type').val()
    @model.set fix_amount: @$('#fix_amount').val()
    @model.set list_item: @$('#list_item').val()
    @model.set range_from: @$('#range_from').val()
    @model.set range_to: @$('#range_to').val()
    @model.set min_t: @$('#min_t').val()
    @model.set max_t: @$('#max_t').val()
    @model.set amount_from: @$('#amount_from').val()
    @model.set amount_to: @$('#amount_to').val()
    @model.set template_id: tempalateUrl;
    @model.save {},
      success: (model) ->
        App.Vent.trigger 'expence:create', model
      error: ->
        alert 'error'
