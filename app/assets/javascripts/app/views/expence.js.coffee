class App.Views.Expence extends Backbone.View

  template: HandlebarsTemplates['app/templates/expence']

  events:
    'click #show_expence_details': 'showExpenceDetails'


  initialize: ->
    @listenTo @model, 'destroy', @remove
    @listenTo @model, 'change', @render

  showExpenceDetails: (e) ->
    e.preventDefault()
    App.Vent.trigger 'expence:show', @model
    isGood()

  render: ->
    @$el.html(@template(@model.toJSON()))
    @
