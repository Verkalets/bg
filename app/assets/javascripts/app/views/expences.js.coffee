class App.Views.Expences extends Backbone.View
  template: HandlebarsTemplates['app/templates/expences']


  events:
    'click a#new_expence': 'newExpence'

  initialize: ->
    @listenTo @collection, 'reset', @render
    @listenTo App.Vent, 'expence:create', @addToCollection
    @listenTo @collection, 'add', @renderExpence
    @collection.fetch({ reset: true })



  newExpence: (e) ->
    e.preventDefault()
    App.Vent.trigger "expence:new"
    item = $('#list_item').html()
    $('#ml').html(item)

  addToCollection: (model) ->
    @collection.add model


  templateUrl = document.URL.toString().substr(document.URL.toString().indexOf("es/") + 3).split("/edit")[0]


  render: ->
    @$el.html(@template())
    filtered_collection = @collection.where({ template_id: templateUrl})
    filtered_collection.forEach @renderExpence, @
    @

  renderExpence: (model) ->
    v = new App.Views.Expence({ model: model})
    @$('ul').append(v.render().el)
