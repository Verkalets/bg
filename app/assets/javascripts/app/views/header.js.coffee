class App.Views.Header extends Backbone.View

  template: HandlebarsTemplates['app/templates/header']


  render: ->
    @$el.html(@template())
    @
