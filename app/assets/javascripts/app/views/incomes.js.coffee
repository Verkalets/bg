class App.Views.Incomes extends Backbone.View
  template: HandlebarsTemplates['app/templates/incomes']

  events:
    'click a#new_income': 'newIncome'

  initialize: ->
    @listenTo @collection, 'reset', @render
    @listenTo App.Vent, 'income:create', @addToCollection
    @listenTo @collection, 'add', @renderIncome
    @collection.fetch({ reset: true })


  newIncome: (e) ->
    e.preventDefault()
    App.Vent.trigger "income:new"

  addToCollection: (model) ->
    @collection.add model


  templateUrl = document.URL.toString().substr(document.URL.toString().indexOf("es/") + 3).split("/edit")[0]


  render: ->
    @$el.html(@template())
    filtered_collection = @collection.where({ template_id: templateUrl})
    filtered_collection.forEach @renderIncome, @
    @

  renderIncome: (model) ->
    v = new App.Views.Income({ model: model})
    @$('ul').append(v.render().el)
