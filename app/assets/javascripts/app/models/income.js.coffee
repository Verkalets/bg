class App.Models.Income extends Backbone.Model
  urlRoot: '/income'


class App.Collections.Incomes extends Backbone.Collection
  model: App.Models.Income
  url: '/income'
