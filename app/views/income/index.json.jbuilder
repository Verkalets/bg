json.array! @incomes do |income|
  json.id income.id
  json.ein income.ein
  json.name_arb income.name_arb
  json.template_id income.template_id
end
