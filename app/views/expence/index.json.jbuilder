json.array! @expences do |e|
  json.id e.id
  json.type e.type
  json.peridiocicy e.peridiocicy
  json.rec_name e.rec_name
  json.rec_type e.rec_type
  json.fix_amount e.fix_amount
  json.range_from e.range_from
  json.range_to e.range_to
  json.list_item e.list_item
  json.min_t e.min_t
  json.max_t e.max_t
  json.amount_from e.amount_from
  json.amount_to e.amount_to
  json.template_id e.template_id
end
