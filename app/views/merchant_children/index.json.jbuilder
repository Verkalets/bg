json.array!(@merchant_children) do |merchant_child|
  json.extract! merchant_child, :id, :name
  json.url merchant_child_url(merchant_child, format: :json)
end
