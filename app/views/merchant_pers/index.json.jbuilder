json.array!(@merchant_pers) do |merchant_per|
  json.extract! merchant_per, :id, :name
  json.url merchant_per_url(merchant_per, format: :json)
end
