json.array!(@templates) do |template|
  json.extract! template, :id, :bank, :account_name, :start_amount, :from_date, :to_date
  json.url template_url(template, format: :json)
end
