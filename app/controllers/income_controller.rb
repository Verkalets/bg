class IncomeController < ApplicationController
  skip_before_action :verify_authenticity_token

  respond_to :json
  def index
    @incomes = Income.all
  end


  def create
    income = Income.new(income_params)
    income.save
    respond_with(income)
  end


  def show
    @income = Income.find(params[:id])
  end


  def destroy
    income = Income.find(params[:id])
    income.destroy
    respond_with(income)
  end


  def update
    income = Income.find(params[:id])
    income.update(income_params)
    respond_with(income)
  end

  private

  def income_params
    params.require(:income).permit(:name_arb, :ein, :template_id)
  end
end
