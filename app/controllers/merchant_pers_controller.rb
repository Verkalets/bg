class MerchantPersController < ApplicationController
  before_action :set_merchant_per, only: [:show, :edit, :update, :destroy]

  # GET /merchant_pers
  # GET /merchant_pers.json
  def index
    @merchant_pers = MerchantPer.all
  end

  # GET /merchant_pers/1
  # GET /merchant_pers/1.json
  def show
  end

  # GET /merchant_pers/new
  def new
    @merchant_per = MerchantPer.new
  end

  # GET /merchant_pers/1/edit
  def edit
    @merchant_per = MerchantPer.new
  end

  # POST /merchant_pers
  # POST /merchant_pers.json
  def create
    @merchant_per = MerchantPer.new(merchant_per_params)

    respond_to do |format|
      if @merchant_per.save
        format.html { redirect_to @merchant_per, notice: 'Merchant per was successfully created.' }
        format.json { render action: 'show', status: :created, location: @merchant_per }
      else
        format.html { render action: 'new' }
        format.json { render json: @merchant_per.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /merchant_pers/1
  # PATCH/PUT /merchant_pers/1.json
  def update
    respond_to do |format|
      if @merchant_per.update(merchant_per_params)
        format.html { redirect_to @merchant_per, notice: 'Merchant per was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @merchant_per.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /merchant_pers/1
  # DELETE /merchant_pers/1.json
  def destroy
    @merchant_per.destroy
    respond_to do |format|
      format.html { redirect_to merchant_pers_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_merchant_per
      @merchant_per = MerchantPer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def merchant_per_params
      params.require(:merchant_per).permit(:name)
    end
end
