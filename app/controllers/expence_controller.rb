class ExpenceController < ApplicationController
  skip_before_action :verify_authenticity_token

  respond_to :json
  
  def index
    @expences = Expence.all
  end

  def show
    @expence = Expence.find(params[:id])
  end

  def create
    expence = Expence.new(expence_params)
    expence.save
    respond_with(expence)
  end


  def destroy
    expence = Expence.find(params[:id])
    expence.destroy
    respond_with(expence)
  end


  def update
    expence = Expence.find(params[:id])
    expence.update(expence_params)
    respond_with(expence)
  end

  private

  def expence_params
    params.require(:expence).permit(:type, :peridiocicy,:rec_name,:fix_amount,:rec_type, :range_from, :range_to,:list_item, :template_id, :min_t, :max_t, :amount_from, :amount_to)
  end


end
