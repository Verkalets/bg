class MerchantChildrenController < ApplicationController
  before_action :set_merchant_child, only: [:show, :edit, :update, :destroy]

  # GET /merchant_children
  # GET /merchant_children.json
  def index
    @merchant_children = MerchantChild.all
  end

  # GET /merchant_children/1
  # GET /merchant_children/1.json
  def show
  end

  # GET /merchant_children/new
  def new
    @merchant_child = MerchantChild.new
  end

  # GET /merchant_children/1/edit
  def edit
  end

  # POST /merchant_children
  # POST /merchant_children.json
  def create
    @merchant_child = MerchantChild.new(merchant_child_params)

    respond_to do |format|
      if @merchant_child.save
        format.html { redirect_to @merchant_child, notice: 'Merchant child was successfully created.' }
        format.json { render action: 'show', status: :created, location: @merchant_child }
      else
        format.html { render action: 'new' }
        format.json { render json: @merchant_child.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /merchant_children/1
  # PATCH/PUT /merchant_children/1.json
  def update
    respond_to do |format|
      if @merchant_child.update(merchant_child_params)
        format.html { redirect_to @merchant_child, notice: 'Merchant child was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @merchant_child.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /merchant_children/1
  # DELETE /merchant_children/1.json
  def destroy
    @merchant_child.destroy
    respond_to do |format|
      format.html { redirect_to merchant_children_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_merchant_child
      @merchant_child = MerchantChild.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def merchant_child_params
      params.require(:merchant_child).permit(:name, :merchant_per_id)
    end
end
